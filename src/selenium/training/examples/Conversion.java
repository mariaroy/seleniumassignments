package selenium.training.examples;

public class Conversion {
	public static void intFloat(int num1)
	{
		int i1=num1;
		float f1=i1;
		System.out.println("Integer is " + i1);
		System.out.println("Converted to float is" + f1);
	}
	public static void intDouble(int num1) {
		int i1=num1;
		double d1=(i1);
		System.out.println("Integer is " + i1);
		System.out.println("Converted to double is" + d1);
	}
	public static void main(String[] args) {
		intFloat(5);
		intDouble(5);
	}

}
