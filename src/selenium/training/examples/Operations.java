package selenium.training.examples;

public class Operations {
	public static void main(String[] a) {
		Calculator cal = new Calculator();
		String operator = a[0]; 
		String operand1 = a[1];
		String operand2 = a[2];
		int number1= Integer.parseInt(operand1);
		int number2= Integer.parseInt(operand2);
		int result =0;
		if (operator.equals("+")) {
			result=cal.add(number1, number2);
		} else if (operator.equals("-")) {
			result=cal.sub(number1, number2);
		} else if (operator == "/") {
			result=cal.div(number1, number2);
		} else if (operator == "*") {
			result=cal.mul(number1, number2);
		}
		System.out.println(result);
	}
}
